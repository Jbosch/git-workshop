# Git commit
> Commit your staged changes

After the previous excursive we now have staged files. However we would like to have these changes *SAVED*.

To do this we need to use the command `git commit`. Commit will save your changes ❗ **LOCALLY**. I'm highlighting locally here as is will not be on the server straight away. All the changes will be a local change. How to upload them will be discussed in the next chapter.

`git commit` needs message, this message will help you and the team to see what you've changed. To add this message to our commit we can use the following command `git commit -m "YOUR_MESSAGE"`. Make sure that your message is descriptive enough so that others now what happened.


##  💪🏻 Exercise 6: 
**Goal**: We are going commit our changes

1. Open up a command prompt/ terminal
2. Navigate to the recent created project.
3. Type in `git status`, what do you see now?
```shell
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   jeffrey_bosch.md

```
4. Type in the command `git commit -m "MESSAGE"` and create a descriptive message, press enter when finished
5. You should see something similar as
```shell
$ git commit -m "adding the jb readme"
[master 0c40466] adding the jb readme
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 jeffrey_bosch.md
```
6. Type in `git status`, what do you see now?
```shell
$ git status
On branch master
nothing to commit, working tree clean
```

**💯 Review time 💯**
Let's see somebody's screen


**Ready for the next part click 👉🏻 [here][task-06]**


[task-06]: ./06.md


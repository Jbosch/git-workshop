# Git add
> Mark changes you have made

After we've edited some, the command `git add` will mark any changes you've made as `staged` also called "ready for committed".

If you then go and make more changes, those new changes will not automatically be staged, even if you’ve changed the same files as before. This is useful for controlling exactly what you commit.

If you’re unsure, just type `git status` to see what’s going on. You’ll see `Changes to be committed` followed by file names. Below that you’ll see `Changes not staged for commit` followed by file names. These are not yet staged.

If you want to have one file `staged` type in `git add <FILE>`, if you would like to see **ALL**  changes to be `staged` type in `git add .`. 

##  💪🏻 Exercise 5: 
**Goal**: We are going stage changes.

1. Open up a command prompt/ terminal
2. Navigate to the recent created project.
3. Type in `git status`, what do you see now?
```shell
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        jeffrey_bosch.md
nothing added to commit but untracked files present (use "git add" to track)
```
4. Let's add `<YOUR_NAME>.md` to the staged list.
5. Type in `git add <YOUR_NAME>.md`
6. Type in `git status`, what do you see now?
```shell
$ git status
On branch master
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        new file:   jeffrey_bosch.md

```

**💯 Review time 💯**
Let's see somebody's screen


**Ready for the next part click 👉🏻 [here][task-05]**


[task-05]: ./05.md


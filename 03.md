# Git branch
> Create a new branch

Branching is a complex(er) topic and I encourage you to read about it as it is a powerful resource when using git.

You can think of a branch as a local checkpoint that has a name. Similar like *Save As* on a file. The new branch is a reference of the current state of your repository. The branch name is used to make our lives easier. By making changes in a branch you will change the state of a branch from a point in time.
Instead of *Save As* git has a command `git branch <NAME>`. This will create a new branch from the state you are at that moment of time.

After we've created a branch we also should navigate to that branch. We can navigate to this branch by using `git checkout <NAME>`. We are now jumping from one branch to a other one. You can think of this as resuming from a point in time, all other changes will be still living in the original branch and we will jump in a new branch and continue but from a different point in time.


##  💪🏻 Exercise 4: 
**Goal**: We are going create a new branch.

1. Open up a command prompt/ terminal
2. Navigate to the recent created project.
3. Type in `git branch <YOUR_NAME>`
4. Type in `git checkout <YOUR_NAME>`
```shell
$ git checkout jeffrey_bosch
Switched to branch 'jeffrey_bosch'
```
***EXTRA***
5. Switch back to the master branch by typing `git checkout master`
6. Let's create a new branch AND switch immediately by typing in `git checkout -b OUR_EXTRA_BRANCH`
```shell
$ git checkout -b OUR_EXTRA_BRANCH
Switched to a new branch 'OUR_EXTRA_BRANCH'
```


**💯 Review time 💯**
Let's see somebody's screen


**Ready for the next part click 👉🏻 [here][task-05]**


[task-05]: ./05.md

